const RippleAPI = require('ripple-lib').RippleAPI


export default class XrpWallet {

    client;
    toVerify;
    verified;

    constructor(url) {
        this.client = new RippleAPI({ server: url })
        this.toVerify = new Map();
        this.verified = []
    }

    generateAddress() {
        const key = this.client.generateAddress();
        const pubkey = key.address;
        const privkey = key.secret;

        return { pubkey, privkey }
    }

    async createClient() {
        this.setListeners()
        try {
            await this.client.connect();
        } catch (error) {
            console.log(error)
        }

        // this.subscribeLedger()
    }

    async preparePayment(from, to, amount, distTag, sourceTag) {
        const payment = {
            "source": {
                "address": from,
                "tag": sourceTag,
                "maxAmount": {
                    "value": "" + amount,
                    "currency": "XRP"
                }
            },
            "destination": {
                "address": to,
                "tag": distTag,
                "amount": {
                    "value": "" + amount,
                    "currency": "XRP"
                }
            }
        };

        const removeEmpty = (obj) => {
            Object.keys(obj).forEach(key =>
                (obj[key] && typeof obj[key] === 'object') && removeEmpty(obj[key]) ||
                (obj[key] === undefined) && delete obj[key]
            );
        };
        removeEmpty(payment)

        return await this.client.preparePayment(from, payment)
    }

    subscribeAccounts(wallets) {
        this.client.connection.request({
            command: 'subscribe',
            accounts: wallets
        })
    }

    listenTransactions() {
        this.client.connection.on('transaction', (ev) => {
            const {
                engine_result,
                ledger_index,
                meta: { TransactionResult, delivered_amount },
                transaction: { Account, Amount, Destination, DestinationTag, Fee, LastLedgerSequence, hash },
                validated } = ev;

            if ("tesSUCCESS" !== engine_result || "tesSUCCESS" !== TransactionResult) {
                this.failCheck(hash)
            } else if (delivered_amount) {
                this.proceedDeliveredAmount(delivered_amount, txHash)
            } else if (!validated) {
                this.validCheck(hash)
            } else {
                this.proceedOk(hash, Account, Destination, Amount, Fee, ledger_index, LastLedgerSequence, DestinationTag)
            }


            console.log(JSON.stringify(ev, null, 2))
        })
    }

    async verifyTx(txHash, minLedger, maxLedger) {
        const tx = await this.client.getTransaction(txHash, {
            minLedgerVersion: minLedger,
            maxLedgerVersion: maxLedger
        })
        console.log(`Tx ${txHash} from Ledger ${minLedger} verified  with result ${tx.outcome.result}`)

        return tx.outcome.result === 'tesSUCCESS'
    }

    async proceedOk(hash, from, to, amount, fee, ledgerMin, ledgerMax, distTag) {
        const txArray = this.toVerify.get(ledgerMin)
        if (txArray) {
            txArray.push({ hash, ledgerMin, ledgerMax, distTag })
        } else {
            this.toVerify.set(ledgerMin, [{ hash, ledgerMin, ledgerMax, distTag }])
        }

        console.log(`Tx ${hash} accepted `)
    }

    async validateAll() {
        const { ledgerVersion: validLedger } = await this.lastValidatedLedger();
        console.log(`Last legder validated version: ${validLedger}`)

        for (const ledger of this.toVerify.keys()) {
            if (validLedger >= ledger) {
                const txArray = this.toVerify.get(ledger)
                if (txArray.length === 0) {
                    this.toVerify.delete(ledger)
                    continue
                }

                console.log(`Start validating for ledger: ${ledger}`)
                let tx;
                while (tx = txArray.shift()) {
                    this.verifyTx(tx.hash, tx.ledgerMin, tx.ledgerMax) && this.verified.push({ hash: tx.hash, tag: tx.distTag })
                }
            }
        }
    }

    async lastValidatedLedger() {
        const { validatedLedger: { hash, ledgerVersion } } = await this.client.getServerInfo()
        return { hash, ledgerVersion };
    }

    failCheck(txHash) {

    }

    validCheck(txHash) {

    }

    proceedDeliveredAmount(delivered_amount, txHash) {

    }

    subscribeLedger() {
        this.client.connection.request({
            command: 'subscribe',
            streams: ['ledger']
        })
    }

    listenLedger() {
        this.client.on('ledger', ledger => {
            console.log(JSON.stringify(ledger, null, 2));
        });
    }

    setListeners() {
        this.client.on('connected', () => {
            console.log('<< connected >>');
        })
        this.client.on('disconnected', (code) => {
            console.log('<< disconnected >> code:', code)
        })
        this.client.on('error', (errorCode, errorMessage) => {
            console.log(errorCode + ': ' + errorMessage)
        })
        this.listenTransactions()
    }

}
