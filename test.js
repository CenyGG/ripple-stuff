import XrpWallet from './XrpWallet'


// process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const url = 'ws://127.0.0.1:6007'

/* Credentials of the account placing the order */
const gen = "rHb9CJAWyB4rj91VRWn96DkukG4bwdtyTh"
const gen_secr = "snoPBrXtMeMyMHUVTgbuqAfg1SUTb"

const hot = "rrhCES4GcQFjPqyrqEi5SgcGgtXeYHCNeR"

const localUser1 = 3085044970
const localUser2 = 472337450

let wallet = new XrpWallet(url);

(async () => {
    await wallet.createClient()

    try {
        setInterval(wallet.validateAll.bind(wallet), 15000)
        wallet.subscribeAccounts([hot])

        // print(await wallet.client.getServerInfo())
        // console.log(await wallet.client.getFee())

        // wallet.client.getTransactions(hot,  {earliestFirst: true, minLedgerVersion: 8378112})
        // .then(print)
        // .catch(print)

        // console.log((await wallet.client.getLedger({ledgerVersion:8378174})).ledgerHash)

        await wait(() => submitTx(gen, gen_secr, gen, 1337, 123, 1234))



    } catch (error) {
        console.error("Err")
        print(error)
    }

})()

async function submitTx(from, secret, to, amount, distTag) {
    const { txJSON, instructions } = await wallet.preparePayment(from, to, amount, distTag)
    const { signedTransaction, id } = await wallet.client.sign(txJSON, secret)
    const { resultCode, resultMessage } = await wallet.client.submit(signedTransaction);

    print({ resultCode, resultMessage })
    return id
}

function print(_obj) {
    console.log(JSON.stringify(_obj, null, 4))
}


async function getAccount(account) {
    return await wallet.client.getAccountInfo(account);
}

async function printAcc(acc) {
    print(await getAccount(acc))
}

async function wait(f) {
    await new Promise((res, rej) => {
        setTimeout(() => { f(); res() }, 2000)
    })
}

